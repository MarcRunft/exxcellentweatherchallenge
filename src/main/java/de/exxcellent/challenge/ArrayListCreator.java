
package de.exxcellent.challenge;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;



/**
 * Stellt statische Methode bereit, um Inhalt einer Datei zeilenweise
 * auszulesen und in eine ArrayList zu schreiben
 *
 * @author Marc Runft <marc.runft@alice-dsl.net>
 */


public class ArrayListCreator{
    
        /* Liest alle Zeilen einer Datei aus und gibt diese als ArrayList zurück.
    *
    *dateipfad: Dateiname der auszulesenden Datei mit Pfad aus Sicht des 
    *           Ressourcen-Ordners
    */
    public static ArrayList<String> getLinesFromFile(String dateipfad){
        //Initialisiere Liste
        ArrayList<String> dat = new ArrayList();
      
	//Greife auf Datei aus Ressourcen-Ordner zu
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        File file;
        file = new File(classLoader.getResource(dateipfad).getFile());
        
	try (Scanner scanner = new Scanner(file)) {
  
            //Nun durchkaemme die komplette Datei. Fuege jede Zeile als String
            //der Liste 'data' hinzu
            while (scanner.hasNextLine()) {
		String line = scanner.nextLine();
		dat.add(line);
            }

            scanner.close();

	} catch (IOException e) {
            System.out.printf("Fehler des Typs IOException");
	}
        
        return dat;
    }
}