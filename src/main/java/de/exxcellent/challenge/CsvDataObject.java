package de.exxcellent.challenge;

import java.util.ArrayList;

/**
 * Klasse zum Auslesen einer CSV-Datei. Stellt verschiedene Methoden bereit,
 * um handliche Arrays zu erhalten.
 *
 * @author Marc Runft <marc.runft@alice-dsl.net>
 */

public class CsvDataObject{
    
    /* Klassenvariable, die intern CSV-Daten speichert, die beim Konstruktur
    ausgelesen werden
    */
    private final ArrayList<String> dataLok;
    private final String trennzeichenLok;
   
    /*KONSTRUKTOR
    * Liest alle Eintraege einer CSV-Datei mit Trennzeichen 'trennzeichen'
    *   aus und speichert sie als ArrayList von Zeilen-String-Arrays in 'data'
    *dateipfad: Dateiname der auszulesenden Datei mit Pfad aus Sicht des 
    *           Ressourcen-Ordners
    *trennzeichen: Trennzeichen fuer die CSV-Datei
    */
    public CsvDataObject(
                String dateipfad, 
                String trennzeichen){
        //Initialisierungen
        dataLok = ArrayListCreator.getLinesFromFile(dateipfad);
        trennzeichenLok = trennzeichen;
    }
    
    /* Methode, die Dateneintrag in gegebener Zeile und Spalte abruft */
    public String eintragStr(int zeilennummer, int spaltennummer){
        String eintrag;
        
        String zeile = (String) dataLok.get(zeilennummer);
        String[] zeileSepariert = zeile.split(trennzeichenLok);
        eintrag = zeileSepariert[spaltennummer];
        
        return eintrag;
    }
    
    /* Methode, die gegebene Spalte ab 'startzeilenindex' im Int-Format
    abruft. Mit 'startzeilenindex' koennen CSV-Header kontrolliert werden.*/   
    public int[] spalteInt(int spaltennummer, int startzeilenindex) {
        int[] spalte = new int[dataLok.size() - startzeilenindex];
        
        for (int lineIndex = 0; lineIndex < dataLok.size()-startzeilenindex; lineIndex ++){
            spalte[lineIndex] = Integer.parseInt(
                    eintragStr(lineIndex + startzeilenindex, spaltennummer));
        }
        
        return spalte;
    }
    
    /* Methode, die gegebene Spalte ab 'startzeilenindex' im Float-Format
    abruft. Mit 'startzeilenindex' koennen CSV-Header kontrolliert werden.*/ 
    public float[] spalteFloat(int spaltennummer, int startzeilenindex){
        float[] spalte = new float[dataLok.size() - startzeilenindex];
        
        for (int lineIndex = 0; lineIndex < dataLok.size()-startzeilenindex; lineIndex ++){
            spalte[lineIndex] = Float.parseFloat(
                    eintragStr(lineIndex + startzeilenindex, spaltennummer));
        }
        
        return spalte;
        
    }
    
    /* Methode, die gegebene Spalte ab 'startzeilenindex' im String-Format
    abruft. Mit 'startzeilenindex' koennen CSV-Header kontrolliert werden.*/ 
    public String[] spalteString(int spaltennummer, int startzeilenindex){
        String[] spalte = new String[dataLok.size() - startzeilenindex];
        
        for (int lineIndex = 0; lineIndex < dataLok.size() - startzeilenindex; lineIndex ++){
            spalte[lineIndex] = eintragStr(lineIndex + startzeilenindex, spaltennummer);
        }
        
        return spalte;
    }
    

}


