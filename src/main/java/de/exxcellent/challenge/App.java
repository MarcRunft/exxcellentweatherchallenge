package de.exxcellent.challenge;

/**
 * Hauptprozess fuer die Weather bzw. Football Challenge.
 *
 * @author Marc Runft <marc.runft@alice-dsl.net>
 */
public final class App {

    public static void main(String[] args){
        //Erstelle CsvDataObject-Objekte, um auf CSV-Daten zuzugreifen
        CsvDataObject weatherData 
                = new CsvDataObject(
                "de/exxcellent/challenge/weather.csv", ",");
        
        CsvDataObject footballData 
                = new CsvDataObject(
                "de/exxcellent/challenge/football.csv", ",");
        
        //Benutze die Methoden der CsvDataObject-Klasse, um Spalten aus den 
        //CSV-Dateien als Arrays zu erhalten
        String[] weatherDays = weatherData.spalteString(0, 1);
        float[] weatherMxt = weatherData.spalteFloat(1, 1);
        float[] weatherMnt = weatherData.spalteFloat(2, 1);
        
        String[] footballTeams = footballData.spalteString(0, 1);
        int[] footballGoals = footballData.spalteInt(5, 1);
        int[] footballGoalsAlwd = footballData.spalteInt(6, 1);
        
        //Erstelle VectorComparer-Objekte, um Spalten jeweils zu vergleichen
        VectorComparer weatherMaxMinComparer 
                = new VectorComparer(weatherMxt, weatherMnt);
        VectorComparer footballGoalsComparer
                = new VectorComparer(footballGoals, footballGoalsAlwd);
        
        //Benutze die VectorComparer-Objekte, um jeweils die Indizes der idealen
        //Tage/Mannschaft zu finden
        int iBestDay = weatherMaxMinComparer.argminAbsDelta();
        int iBestTeam = footballGoalsComparer.argminAbsDelta();
        
        //Ermittle daraus den/die ideale(n) Tag/Mannschaft
        String dayWithSmallestTempSpread = weatherDays[iBestDay];
        String teamWithSmallesGoalSpread = footballTeams[iBestTeam];
        
        System.out.printf("Day with smallest temperature spread : %s%n", dayWithSmallestTempSpread);
        System.out.printf("Team with smallest goal spread       : %s%n", teamWithSmallesGoalSpread);
        
    }
}
