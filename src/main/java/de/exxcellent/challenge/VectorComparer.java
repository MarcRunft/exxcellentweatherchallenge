package de.exxcellent.challenge;

/**
 * Diese Klasse dient dazu, zwei Arrays der Typen int oder float zu vergleichen
 *
 * @author Marc Runft <marc.runft@alice-dsl.net>
 */

public class VectorComparer{
    
    private final float[] v1;
    private final float[] v2;
    private final float[] delta;
    private final int length;
    
    //float-Konstruktor
    //erstellt VectorComparer-Objekt aus float-Arrays vector1 und vector2
    public VectorComparer(float[] vector1, float[] vector2){
        //Initialisieren
        v1 = vector1;
        v2 = vector2;
              
        //Beim Vergleich nur so viele Elemente beruecksichtigen, wie bei beiden
        //Vektoren vorhanden
        int l1 = v1.length;
        int l2 = v2.length;
        length = Math.min(l1,l2);
        delta = new float[length];
        
        //delta bestimmen
        for (int i=0; i<length; i++){
            delta[i] = v2[i] - v1[i];
        }
    } 
    
    //Int-Konstruktur
    public VectorComparer(int[] vector1, int[] vector2){
        v1 = new float[vector1.length];
        v2 = new float[vector2.length];
        
        //Beim Vergleich nur so viele Elemente beruecksichtigen, wie bei beiden
        //Vektoren vorhanden
        int l1 = v1.length;
        int l2 = v2.length;
        length = Math.min(l1,l2);
        delta = new float[length];
        
        //v1, v2, delta bestimmen
        for (int i=0; i<l1; i++){
            v1[i] = (float) vector1[i];
        }
        for (int i=0; i<l2; i++){
            v2[i] = (float) vector2[i];
        }
        for (int i=0; i<length; i++){
            delta[i] = v2[i] - v1[i];
        }
    }
    
    //Methode, die den Index des betragsmaessig kleinsten delta bestimmt
    public int argminAbsDelta(){
        int argmin = 0;
        float min = 0;
        if (length == 0){
            return 0;
        }
        else{
            //Initialisierung
            min = Math.abs(delta[0]);
            //Finde kleinere Eintraege in 'delta' als min.
            for(int i=1; i<length; i++){
                if (Math.abs(delta[i]) < min){
                    argmin = i;
                    min = Math.abs(delta[i]);
                }
            }
            return argmin;
        }
    }
}